package com.vova;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
	private static Logger logger1 = LogManager.getLogger(Application.class);

	public static void main(String[] args) {
		logger1.trace("Trace message");
		logger1.debug("Debug message");
		logger1.info("Info message");
		logger1.warn("Warn message");
		logger1.error("Error message");
		logger1.fatal("Fatal message");
	}

}
